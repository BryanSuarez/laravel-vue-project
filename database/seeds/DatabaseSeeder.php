<?php

use App\Customer;
use App\Post;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(PostsTableSeeder::class);
        factory(Post::class, 50)->create();
        factory(Customer::class, 200)->create();
    }
}
